# Import Regelrapporten

Verkenning importeren van iStandaard Referentie-regels om deze te converteren naar Markdown met Front Matter variabelen. 

Via de volgende stappen kan de verkenning worden geevalueerd:

1. Uploaden van Regelrapporten in .XLSX-formaat naar de folder [regelrapporten](regelrapporten)

2. De import kan worden gestart via [GitLab Web Pipeline Interface](https://gitlab.com/istddevops/exploration/regels/import-regelrapporten/-/pipelines)

3. Daarna kan het resultaat worden geraadpleegd via de gegenereerde [HUGO Book Theme Pages Site](https://istddevops.gitlab.io/exploration/regels/import-regelrapporten)

## Doelstelling verkenning

Doel is het achterhalen of de huidige iStandaard Regelrapporten kunnen worden geconverteerd naar een generiek herbruikbaar formaat waarmee publicaties kunnen worden generereerd.

## Implementatie verkenning

De verkenning bouwt voort op de Regelrapporten zoals deze momenteel voor de iStandaarden worden gepubliceerd. De volgende onderdelen zijn daarbij toegepast en/of ontwikkeld:

- Maatwerk op basis dit [Python-script](regelrapporttohugomd.py) 
