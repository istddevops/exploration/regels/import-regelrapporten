#
# Convert Regels Raport sheet to HUGO Content Markdown format
#

# packageimports
from openpyxl import load_workbook
import os
import sys
from humps import camelize

# iStandaard Regel Class
class IstdRegel:
    def __init__(self):
        self.type: str = None
        self.code: str = None
        self.titel: str = None
        self.documentatie: str = None
        self.XSDRestrictie = None
        self.retourcode = None
        self.controleniveau = None
        self.referentieregel = None

# create HUGO Markdown content / docs path and index files for iStandaard Version
def createIstdNameVersionIndexPathFiles(istdName: str, istdVersion: str):
    # Initialize directory and Index-file variables
    istdNamePath = os.path.join(os.getcwd(), 'content', 'docs', istdName)
    istNameIndexFilePath = os.path.join(istdNamePath, '_index.md')
    istdNameVersionPath = os.path.join(istdNamePath, istdVersion)
    istNameVersionIndexFilePath = os.path.join(istdNameVersionPath, '_index.md')
    # Create directory and Index-files
    os.makedirs(istdNameVersionPath, exist_ok=True)
    # Create iStandaard Name Index-file
    with open(istNameIndexFilePath, 'w') as istdNameIndexFile:
        print('---', file=istdNameIndexFile)
        print('bookToC: false', file=istdNameIndexFile)
        print('bookCollapseSection: true', file=istdNameIndexFile)
        print('title: ' + istdName, file=istdNameIndexFile)
        print('description: Geconverteerd vanuit Regelrapport', file=istdNameIndexFile)
        print('---', file=istdNameIndexFile)
        print('', file=istdNameIndexFile)
        print('{{< sectiontable >}}', file=istdNameIndexFile)
    # Create iStandaard Name Version Index-file
    with open(istNameVersionIndexFilePath, 'w') as istdNameVersionIndexFile:
        print('---', file=istdNameVersionIndexFile)
        print('bookToC: false', file=istdNameVersionIndexFile)
        print('bookCollapseSection: true', file=istdNameVersionIndexFile)
        print('title: ' + istdVersion, file=istdNameVersionIndexFile)
        print('description: Geconverteerd vanuit Regelrapport', file=istdNameVersionIndexFile)
        print('---', file=istdNameVersionIndexFile)
        print('', file=istdNameVersionIndexFile)
        print('{{< sectiontable >}}', file=istdNameVersionIndexFile)

# retuns HUGO Markdown content / docs path for iStandaard Family Name / Version / regelType
def docsRegelPath(istdName: str, istdVersion: str, regelType: str):
    return os.path.join(os.getcwd(), 'content', 'docs', istdName, istdVersion, regelType.lower())

# return string between single quotes
def txtValue(inputStr: str):
    if inputStr != None:
        return inputStr
    else:
        return ""

# write HUGO Markdown content Index File for regeType
def initRegelType(istdName: str, istdVersion: str, regelType: str):
    regelPath = docsRegelPath(istdName, istdVersion, regelType)
    os.makedirs(regelPath, exist_ok=True)
    indexFilePath = os.path.join(regelPath, '_index.md')
    if not os.path.isfile(indexFilePath):
        with open(indexFilePath, 'w') as indexFile:
            print('---', file=indexFile)
            print('bookToC: false', file=indexFile)
            print('bookCollapseSection: true', file=indexFile)
            print('title: ' + regelType, file=indexFile)
            print('description: Geconverteerd vanuit Regelrapport', file=indexFile)
            print('---', file=indexFile)
            print('', file=indexFile)
            print('{{< sectiontable >}}', file=indexFile)

# write HUGO Markdown content Regel Files for regeType
def writeRegelsToHugoMdContentFiles(istdName: str, istdVersion: str, regels = []):
    for regel in regels:
        # write regel file
        initRegelType(istdName, istdVersion, regel.type)
        regelPath = docsRegelPath(istdName, istdVersion, regel.type)
        regelFile = regel.code.lower() + '.md'
        regelFilePath = os.path.join(regelPath, regelFile)
        with open(regelFilePath, 'w') as regelFile:
            # Markkdown Header
            print('---', file=regelFile)
            # Mandatorty Variables
            print('bookToC: false', file=regelFile)
            print('regelType: ' + regel.type, file=regelFile)
            print('regelCode: ' + regel.code, file=regelFile)
            print('description: |\n  ' + txtValue(regel.titel.split(regel.code + ': ')[1]), file=regelFile)
            # Optional Variables
            if regel.XSDRestrictie != None:
                print('XSDRestrictie: ' + regel.XSDRestrictie, file=regelFile)
            if regel.retourcode != None:
                print('retourcode: ' , file=regelFile)
                retourcodeLijst = regel.retourcode.split(',')
                for retourcodeItem in retourcodeLijst:
                    print('- ' + retourcodeItem.strip(), file=regelFile)
            if regel.controleniveau != None:
                print('controleniveau: ' + regel.controleniveau, file=regelFile)
            if regel.referentieregel != None:
                print('referentieregel: ' + regel.referentieregel, file=regelFile)
            print('---', file=regelFile)
            # Markkdown Body
            print('{{< hint warning >}}', file=regelFile)
            print('  ', file=regelFile)
            if regel.documentatie != None:
                print(regel.documentatie, file=regelFile)
            else:
                print('(geen documentatie)', file=regelFile)
            print('  ', file=regelFile)
            print('{{< /hint >}}', file=regelFile)

# convert Regel Rapport Excel File to HUGO Markdown Content Files
def convertRegelrapportXlsToHugoContent(regelRapportFilePath: str):
    # Parse iStandaard name and version values from Rapport File Path
    regelRapportFileParts = os.path.split(regelRapportFilePath)[1].split(' ')
    istdName = regelRapportFileParts[1]
    istdVersion = regelRapportFileParts[2].split('.xlsx')[0]
    # Create Path and Index-files to support Go Hugo Publication
    createIstdNameVersionIndexPathFiles(istdName, istdVersion)
    # Load excel worksheet to process
    wb = load_workbook(filename=regelRapportFilePath)
    ws = wb['Regels']
    columnNames = []
    regels = []
    rowNr: int = 0

    # process every row in the worksheet
    for wsRow in ws:
        if rowNr < 1:
            # add column names from the first row
            for wsCell in wsRow:
                columnNames.append(camelize(wsCell.value))
        else:
            # get data row values for each Regel
            columnNr = 0
            # create new iStandaard regel instance
            regel = IstdRegel()
            # process cell value for every column in the row
            for wsCell in wsRow:
                # map column cell values to regel instance
                columnName: str = columnNames[columnNr]
                if columnName == 'type':
                    regel.type = wsCell.value
                elif columnName == 'regelcode':
                    regel.code = wsCell.value
                elif columnName == 'titel':
                    regel.titel = wsCell.value
                elif columnName == 'documentatie':
                    regel.documentatie = wsCell.value
                elif columnName == 'XSDRestrictie':
                    regel.XSDRestrictie = wsCell.value
                elif columnName == 'retourcode':
                    regel.retourcode = wsCell.value
                elif columnName.endswith('niveau'):
                    regel.controleniveau = wsCell.value
                elif columnName == 'referentieregel':
                    regel.referentieregel = wsCell.value
                else:
                    print('ColumnName Not Mapped: ==> ' + columnName)
                # next column to process
                columnNr = columnNr + 1
            # add processed regel instance
            regels.append(regel)
        # process next row
        rowNr = rowNr + 1

    writeRegelsToHugoMdContentFiles(istdName, istdVersion, regels)

# Main Conversion Script
#convertRegelrapportXlsToHugoContent(sys.argv[1])

# Main Conversion Script
inputPath = sys.argv[1]
print(inputPath)

# process xlsx files
for inputFile in os.listdir(inputPath):
    if inputFile.startswith('Regelrapport') and inputFile.endswith('.xlsx'):
        regelExportFilePath = os.path.join(inputPath, inputFile)
        print(regelExportFilePath)
        convertRegelrapportXlsToHugoContent(regelExportFilePath)
